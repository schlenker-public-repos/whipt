<?php

namespace Whipt\Frontend;

use Whipt\Common\Assets as Common_Assets;
use Whipt\PluginData as PluginData;

// Abort if this file is called directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( Assets::class ) ) {
	/**
	 * Enqueues the public-facing assets.
	 */
	class Assets {

		/**
		 * @var Common_Assets
		 */
		var $common_assets;

		/**
		 * @var Frontend_Assets
		 */
		var $frontend_assets;

		public function __construct() {
			$this->common_assets = new Common_Assets();
			$manifest = $this->common_assets->get_asset_manifest();
			$this->frontend_assets = $manifest->frontend;
		}

		/**
		 * Register and enqueue the stylesheets for the public-facing side of the site.
		 *
		 * Must register before we enqueue!
		 */
		public function enqueue_styles(): void {
			$file_name = $this->frontend_assets->css;

			$registered = $this->common_assets->register_style( $file_name, 'frontend' );

			if ( $registered ) {
				$this->common_assets->enqueue_style( $file_name, 'frontend' );
			}
		}

		/**
		 * Register and enqueue the scripts for the public-facing side of the site.
		 *
		 * Must register before we enqueue!
		 */
		public function enqueue_scripts(): void {
			global $wp_scripts;
			$file_name = $this->frontend_assets->js;

			$registered = $this->common_assets->register_script(
				$file_name,
				'frontend',
				[ 'jquery' ]
			);

			if ( $registered ) {
				$this->common_assets->enqueue_script( $file_name, 'frontend' );
			}
		}
	}
}
