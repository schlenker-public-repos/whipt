<?php

declare( strict_types=1 );

namespace Whipt\Admin;

use Whipt\Common\Assets as Common_Assets;
use Whipt\PluginData as PluginData;

// Abort if this file is called directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( Assets::class ) ) {
	/**
	 * Enqueues the global admin assets.
	 *
	 * Settings Page adds additional.
	 */
	class Assets {

		/**
		 * @var Common_Assets
		 */
		var $common_assets;

		/**
		 * @var Admin_Assets
		 */
		var $admin_assets;

		public function __construct() {
			$this->common_assets = new Common_Assets();
			$manifest = $this->common_assets->get_asset_manifest();
			$this->admin_assets = $manifest->admin;
		}

		/**
		 * Register and enqueue the stylesheets for every admin area.
		 *
		 * Must register before we enqueue!
		 */
		public function enqueue_styles(): void {
			$file_name = $this->admin_assets->css;

			$registered = $this->common_assets->register_style( $file_name, 'admin' );

			if ( $registered ) {
				$this->common_assets->enqueue_style( $file_name, 'admin' );
			}
		}

		/**
		 * Register and enqueue the JavaScript for every admin area.
		 *
		 * Must register before we enqueue!
		 */
		public function enqueue_scripts(): void {
			$file_name = $this->admin_assets->js;

			$registered = $this->common_assets->register_script(
				$file_name,
				'admin',
				[ 'jquery' ]
			);

			if ( $registered ) {
				$this->common_assets->enqueue_script( $file_name, 'admin' );
			}
		}
	}
}