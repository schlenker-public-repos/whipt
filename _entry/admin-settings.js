const importAll = (r) => r.keys().forEach(r);

importAll(require.context('../src/Admin/Settings/pcss/', true, /\.pcss$/));
importAll(require.context('../src/Admin/Settings/js/', true, /\.jsx$/));
