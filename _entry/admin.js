const importAll = (r) => r.keys().forEach(r);

importAll(require.context('../src/Admin/pcss/', true, /\.pcss$/));
importAll(require.context('../src/Admin/js/', true, /\.js$/));
importAll(require.context('../src/Common/pcss/', true, /\.pcss$/));
importAll(require.context('../src/Common/js/', true, /\.js$/));
