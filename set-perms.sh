#!/bin/bash
#
# The purpose of this script is to set directory and file ownership and
# permissions on the root of a plugin or theme that runs in a local
# Docker container. The script opens up the permissions so that the web server user
# (assumed to be www-data) can create or edit files and directories as needed. This 
# script is not to be run on a production website. It is intended to be run in development
# environments only.
#

# Errors are fatal
set -e

# Set core file mode to false. This must be done to prevent git from thinking that all
# files have been changed after the permissions of all directories has been opened
# up to 777.
echo "# "
echo "# Setting core file mode..."
git config core.fileMode false
echo "# "
echo "# Done!"
echo "# "

# Change ownership of all files and folders in the current 
# directory except the node_modules and vendor directories
echo "# Setting directory and file ownership..."
sudo find ./wp-content ./wp-admin ./wp-includes \
   \( -name node_modules \
      -o -name vendor \
   \) -prune \
   -o -exec sudo chown www-data:www-data {} \;
echo "# "
echo "# Done!"
echo "# "

# Change permissions of all directories in the current 
# directory excluding all files and the node_modules and vendor directories
echo "# Setting directory permissions..."
sudo find ./wp-content ./wp-admin ./wp-includes \
   \( -name node_modules \
      -o -name vendor \
   \) -prune \
   -o -type d -exec chmod 777 {} \;
echo "# "
echo "# Done!"
echo "# "

# Change permissions of all files in the current directory 
# except for the files in the node_modules and vendor directories
echo "# Setting file permissions..."
sudo find ./wp-content ./wp-admin ./wp-includes \
   \( -name node_modules \
      -o -name vendor \
   \) -prune \
   -o -type f -exec chmod 666 {} \;
echo "# "
echo "# Done!"
echo "# "
echo "# Directory and file ownership and permissions successfully set!"
echo "# "
